# Better Hero Mechs

Improves some existing hero mechs and adds a few new ones

* All mechs in this mod implement the [baseCoolingRate Fix](https://www.nexusmods.com/mechwarrior5mercenaries/mods/371)

* Modifies Atlas Boar's Head
  * Upgrades engine heat sinks to doubles
  * Upgrades chassis to endo steel
  * Increases ballistic hardpoint to large
  * Adds two laser hardpoints
  * Adds one missile hardpoint
  * Adds BAP and ECM hardpoints
  * Adds AMS hardpoint

* Modifies Highlander Heavy metal
  * Increases engine to 360XL
  * Adds additional energy hardpoints
  * Adds BAP and ECM hardpoints
  * Removes jump jets

* Adds Nightstar NSR-9CM - a faster variant with BAP and more energy slots.

* Adds Marauder II MAD-5G - trades jump jets for a slightly faster engine. Carries a gauss rifle backed by an array of lasers and SRMs.

## Using this mod in Mechwarrior 5

This mod hasn't been uploaded anywhere. You'll need to cook the files yourself.

## Using these files in your own mod

Feel free to use these files in your own Mechwarrior 5 mods! Please give credit to the author `moddingisthegame`.

1. Purchase [Mechwarrior 5 for PC](https://www.epicgames.com/store/en-US/p/mechwarrior-5)
2. Install the [Mechwarrior 5 Modding Toolkt](https://www.epicgames.com/store/en-US/p/mechwarrior-5--mod-editor)
3. Install [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
4. Navigate to the Mechwarrior 5 Editor Plugins directory (typically C:\Program Files\Epic Games\MechWarrior5Editor\MW5Mercs\Plugins) in your terminal of choice
5. `git clone https://gitlab.com/mechwarrior5mods/mechs/betterheromechs.git`
6. Launch the Mechwarrior 5 Modding Toolkit.
7. Click the dropdown next to Manage Mod and select `betterheromechs`
8. Mod away!

## Compatibility

* This mod will conflict with other mods that modify any of the following mech variants:
  * Atlas Boar's Head
  * Highlander Heavy metal
* Will conflict with mods that alter the Marauder II's hardpoints (HPS) file
